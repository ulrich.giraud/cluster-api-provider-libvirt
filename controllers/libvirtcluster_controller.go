/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	goctx "context"

	"github.com/pkg/errors"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/cluster-api/util/annotations"
	"sigs.k8s.io/cluster-api/util/patch"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	ctrlutil "sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
	"sigs.k8s.io/cluster-api/util"
	"sigs.k8s.io/cluster-api/util/conditions"

	libvirt "libvirt.org/libvirt-go"

	infrastructurev1alpha1 "gitlab.com/ulrich.giraud/cluster-api-provider-libvirt/api/v1alpha1"
	"gitlab.com/ulrich.giraud/cluster-api-provider-libvirt/pkg/context"
	pkgutil "gitlab.com/ulrich.giraud/cluster-api-provider-libvirt/pkg/util"
)

// LibvirtClusterReconciler reconciles a LibvirtCluster object
type LibvirtClusterReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=libvirtclusters,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=libvirtclusters/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=libvirtclusters/finalizers,verbs=update
//+kubebuilder:rbac:groups=cluster.x-k8s.io,resources=clusters;clusters/status,verbs=get;list;watch
//+kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the LibvirtCluster object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.11.0/pkg/reconcile
func (r *LibvirtClusterReconciler) Reconcile(ctx goctx.Context, req ctrl.Request) (_ ctrl.Result, reterr error) {
	logger := log.FromContext(ctx)

	cluster := &infrastructurev1alpha1.LibvirtCluster{}
	if err := r.Get(ctx, req.NamespacedName, cluster); err != nil {
		if apierrors.IsNotFound(err) {
			logger.V(4).Info("LibvirtCluster not found, won't reconcile", "key", req.NamespacedName)
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	capiCluster, err := util.GetOwnerCluster(ctx, r.Client, cluster.ObjectMeta)
	if err != nil {
		return ctrl.Result{}, err
	}
	if capiCluster == nil {
		logger.Info("Waiting for Cluster Controller to set OwnerRef on LibvirtCluster")
		return reconcile.Result{}, nil
	}
	if annotations.IsPaused(capiCluster, cluster) {
		logger.V(4).Info("LibvirtCluster %s/%s linked to a cluster that is paused",
			cluster.Namespace, cluster.Name)
		return reconcile.Result{}, nil
	}

	// Create the patch helper.
	patchHelper, err := patch.NewHelper(cluster, r.Client)
	if err != nil {
		return reconcile.Result{}, errors.Wrapf(
			err,
			"failed to init patch helper for %s %s/%s",
			cluster.GroupVersionKind(),
			cluster.Namespace,
			cluster.Name)
	}

	// Create the cluster context for this request.
	clusterContext := &context.ClusterContext{
		Context:        ctx,
		Cluster:        capiCluster,
		LibvirtCluster: cluster,
		Logger:         ctrl.Log.WithName(req.Namespace).WithName(req.Name),
		PatchHelper:    patchHelper,
	}

	// Always issue a patch when exiting this function so changes to the
	// resource are patched back to the API server.
	defer func() {
		if err := clusterContext.Patch(); err != nil {
			if reterr == nil {
				reterr = err
			}
			clusterContext.Logger.Error(err, "patch failed", "cluster", clusterContext.String())
		}
	}()

	// Handle deleted clusters
	if !cluster.DeletionTimestamp.IsZero() {
		return r.reconcileDelete(clusterContext)
	}

	// Handle non-deleted clusters
	return r.reconcileNormal(clusterContext)
}

func (r *LibvirtClusterReconciler) reconcileNormal(ctx *context.ClusterContext) (reconcile.Result, error) {
	// If the LibvirtCluster doesn't have our finalizer, add it.
	ctrlutil.AddFinalizer(ctx.LibvirtCluster, infrastructurev1alpha1.ClusterFinalizer)

	if err := r.reconcileSSHKey(ctx); err != nil {
		conditions.MarkFalse(ctx.LibvirtCluster, infrastructurev1alpha1.LibvirtAvailableCondition, infrastructurev1alpha1.LibvirtUnreachableReason, clusterv1.ConditionSeverityError, err.Error())
		return reconcile.Result{}, err
	}
	conn, err := libvirt.NewConnect(ctx.LibvirtCluster.Spec.LibvirtConnection.ConnectionString())
	if err != nil {
		conditions.MarkFalse(ctx.LibvirtCluster, infrastructurev1alpha1.LibvirtAvailableCondition, infrastructurev1alpha1.LibvirtUnreachableReason, clusterv1.ConditionSeverityError, err.Error())
		ctx.LibvirtCluster.Status.Ready = false
		return ctrl.Result{}, err
	} else {
		conditions.MarkTrue(ctx.LibvirtCluster, infrastructurev1alpha1.LibvirtAvailableCondition)
		ctx.LibvirtCluster.Status.Ready = true
		ctx.Logger.Info("Succesfully connected to libvirt!")
	}
	conn.Close()

	return ctrl.Result{}, nil
}

func (r *LibvirtClusterReconciler) reconcileDelete(ctx *context.ClusterContext) (reconcile.Result, error) {
	// Cluster is deleted so remove the finalizer.
	ctrlutil.RemoveFinalizer(ctx.LibvirtCluster, infrastructurev1alpha1.ClusterFinalizer)

	return reconcile.Result{}, nil
}

func (r *LibvirtClusterReconciler) reconcileSSHKey(ctx *context.ClusterContext) error {
	if isSSHDriver(string(ctx.LibvirtCluster.Spec.LibvirtConnection.Driver)) {
		if ctx.LibvirtCluster.Spec.LibvirtConnection.SSHPrivateKeyRef.Name != "" {
			secret := &corev1.Secret{}
			if err := r.Get(ctx, types.NamespacedName{Namespace: ctx.LibvirtCluster.Namespace, Name: ctx.LibvirtCluster.Spec.LibvirtConnection.SSHPrivateKeyRef.Name}, secret); err != nil {
				return err
			}
			key := pkgutil.SSHKey{
				Name: ctx.LibvirtCluster.Name + "-" + ctx.LibvirtCluster.Spec.LibvirtConnection.SSHPrivateKeyRef.Key + "-id",
				Key:  string(secret.Data[ctx.LibvirtCluster.Spec.LibvirtConnection.SSHPrivateKeyRef.Key]),
			}

			ctx.Logger.V(4).Info("SSH key for %s/%s will be written in %s", ctx.LibvirtCluster.Namespace, ctx.LibvirtCluster.Name, key.Path())
			if err := key.CreatePrivateKey(); err != nil {
				return err
			}
		} else {
			return errors.New("SSH driver: No SSH Key in cluster configuration")
		}
	}

	return nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *LibvirtClusterReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&infrastructurev1alpha1.LibvirtCluster{}).
		Complete(r)
}

func isSSHDriver(driver string) bool {
	switch driver {
	case
		"ssh",
		"libssh",
		"libssh2":
		return true
	}
	return false
}
