package v1alpha1

import (
	"strconv"
)

//+kubebuilder:validation:Enum=qemu;test
//+kubebuilder:default=qemu
type Driver string

//+kubebuilder:validation:Enum=ssh;libssh;libssh2;tcp
//+kubebuilder:default=ssh
type Protocol string

// LibvirtConnection describe all parameters to access to a libvirt daemon
type LibvirtConnection struct {
	// Libvirt driver used on the host. See https://libvirt.org/uri.html for details
	Driver Driver `json:"driver"`

	// Protocol to use to connect to the remote host. See https://libvirt.org/uri.html for details
	// +optional
	Protocol Protocol `json:"protocol,omitempty"`

	// Username to use for remote host connection. See https://libvirt.org/uri.html for details
	// +optional
	Username string `json:"username,omitempty"`

	// Hostname to connect, may be system, session or a distant host. See https://libvirt.org/uri.html for details
	// +optional
	Hostname string `json:"hostname"`

	// Port is the remote port listening for the protocol used. See https://libvirt.org/uri.html for details
	// +kubebuilder:validation:Minimum=1
	// +kubebuilder:validation:Maximum=65535
	// +optional
	Port int `json:"port,omitempty"`

	// Path is the remote host absolute path context. See https://libvirt.org/uri.html for details
	Path string `json:"path"`

	// ExtraParameters for the connection. See https://libvirt.org/uri.html for details
	// +optional
	ExtraParameters map[string]string `json:"extraParameters,omitempty"`

	// SSHPrivateKeyRef is a reference to a ssh private key in a Kubernetes Secret.
	// key path for keyfile ExtraParameter will be : /home/libvirt/.ssh/<cluster-name>-<SSHPrivateKeyRef.key>-id
	// +optional
	SSHPrivateKeyRef SecretKeyRef `json:"sshPrivateKeyRef,omitempty"`
}

func (c *LibvirtConnection) ConnectionString() (ret string) {
	ret = string(c.Driver)

	if c.Protocol != "" {
		ret += "+" + string(c.Protocol)
	}

	ret += "://"

	if c.Username != "" {
		ret += c.Username + "@"
	}

	ret += c.Hostname

	if c.Port != 0 {
		ret += ":" + strconv.Itoa(c.Port)
	}

	if c.Path != "" {
		ret += c.Path
	}

	if len(c.ExtraParameters) != 0 {
		ret += "?"
		first := true
		for key, value := range c.ExtraParameters {
			if !first {
				ret += "&"
			}
			ret += key + "=" + value
			first = false
		}
	}

	return
}
