/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
)

const (
	// ClusterFinalizer allows ReconcileLibvirtCluster to clean up Libvirt
	// resources associated with LibvirtCluster before removing it from the
	// API server.
	ClusterFinalizer = "libvirtcluster.infrastructure.cluster.x-k8s.io"
)

// LibvirtClusterSpec defines the desired state of LibvirtCluster
type LibvirtClusterSpec struct {
	// LibvirtConnection contains parameters to access a libvirt daemon
	LibvirtConnection LibvirtConnection `json:"libvirtConnection"`

	// ControlPlaneEndpoint represents the endpoint used to communicate with the control plane.
	// +optional
	ControlPlaneEndpoint APIEndpoint `json:"controlPlaneEndpoint"`
}

// LibvirtClusterStatus defines the observed state of LibvirtCluster
type LibvirtClusterStatus struct {
	// +optional
	Ready bool `json:"ready,omitempty"`

	// Conditions defines current service state of the LibvirtCluster.
	// +optional
	Conditions clusterv1.Conditions `json:"conditions,omitempty"`

	// FailureDomains is a list of failure domain objects synced from the infrastructure provider.
	FailureDomains clusterv1.FailureDomains `json:"failureDomains,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:printcolumn:name="Ready",type="string",JSONPath=".status.ready",description="Cluster infrastructure is ready for libvirtMachine"
//+kubebuilder:printcolumn:name="Host",type="string",JSONPath=".spec.libvirtConnection.Hostname",description="Hostname is the address of the libvirt endpoint."
//+kubebuilder:printcolumn:name="ControlPlaneEndpoint",type="string",JSONPath=".spec.controlPlaneEndpoint[0]",description="API Endpoint",priority=1

// LibvirtCluster is the Schema for the libvirtclusters API
type LibvirtCluster struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   LibvirtClusterSpec   `json:"spec,omitempty"`
	Status LibvirtClusterStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// LibvirtClusterList contains a list of LibvirtCluster
type LibvirtClusterList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []LibvirtCluster `json:"items"`
}

func (c *LibvirtCluster) GetConditions() clusterv1.Conditions {
	return c.Status.Conditions
}

func (c *LibvirtCluster) SetConditions(conditions clusterv1.Conditions) {
	c.Status.Conditions = conditions
}

func init() {
	SchemeBuilder.Register(&LibvirtCluster{}, &LibvirtClusterList{})
}
