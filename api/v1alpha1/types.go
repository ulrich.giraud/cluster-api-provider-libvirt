/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

// APIEndpoint represents a reachable Kubernetes API endpoint.
type APIEndpoint struct {
	// The hostname on which the API server is serving.
	Host string `json:"host"`

	// The port on which the API server is serving.
	//+kubebuilder:default=6443
	Port int32 `json:"port"`
}

// SecretKeyRef represents a reference to a key in a secret in the same namespace
type SecretKeyRef struct {
	// Name is the name of the Kubernetes Secret
	Name string `json:"name"`

	// Key is the key within the secret referenced
	Key string `json:"key"`
}
