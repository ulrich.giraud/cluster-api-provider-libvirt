package util

import (
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCreateOrUpdateFileInexistentFile(t *testing.T) {
	dir, err := os.MkdirTemp("", "util_test")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(dir)

	err = CreateOrUpdateFile("test\nfile\ncontent", os.FileMode(0600), filepath.Join(dir, "tmpfile"))

	assert.Nil(t, err, "CreateOrUpdateFile failed")

	f, err := os.Open(filepath.Join(dir, "tmpfile"))
	assert.Nil(t, err, "Can't open generated file")
	stat, _ := f.Stat()

	data, err := os.ReadFile(filepath.Join(dir, "tmpfile"))
	assert.Nil(t, err, "Can't read generated file")

	assert.Equal(t, os.FileMode(0600), stat.Mode().Perm())
	assert.Equal(t, "test\nfile\ncontent", string(data))
}

func TestCreateOrUpdateFileExistingSameFile(t *testing.T) {
	dir, err := os.MkdirTemp("", "util_test")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(dir)

	err = CreateOrUpdateFile("test\nfile\ncontent", os.FileMode(0600), filepath.Join(dir, "tmpfile"))
	assert.Nil(t, err, "CreateOrUpdateFile failed")

	f, err := os.Open(filepath.Join(dir, "tmpfile"))
	assert.Nil(t, err, "Can't open generated file")
	stat, _ := f.Stat()
	modTime := stat.ModTime()
	f.Close()

	time.Sleep(1 * time.Second)

	err = CreateOrUpdateFile("test\nfile\ncontent", os.FileMode(0600), filepath.Join(dir, "tmpfile"))
	assert.Nil(t, err, "CreateOrUpdateFile failed")

	f, err = os.Open(filepath.Join(dir, "tmpfile"))
	assert.Nil(t, err, "Can't open generated file")
	stat, _ = f.Stat()

	data, err := os.ReadFile(filepath.Join(dir, "tmpfile"))
	assert.Nil(t, err, "Can't read generated file")

	assert.Equal(t, os.FileMode(0600), stat.Mode().Perm())
	assert.Equal(t, modTime, stat.ModTime())
	assert.Equal(t, "test\nfile\ncontent", string(data))
}

func TestCreateOrUpdateFileInvalidName(t *testing.T) {
	dir, err := os.MkdirTemp("", "util_test")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(dir)

	err = CreateOrUpdateFile("test\nfile\ncontent", os.FileMode(0600), filepath.Join(dir, "tmpfile\000"))
	assert.Error(t, err)
	assert.Equal(t, "*fs.PathError", reflect.TypeOf(err).String())
}
