/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package context

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	clusterv1 "sigs.k8s.io/cluster-api/api/v1beta1"
	"sigs.k8s.io/cluster-api/util/conditions"
	"sigs.k8s.io/cluster-api/util/patch"

	infrav1 "gitlab.com/ulrich.giraud/cluster-api-provider-libvirt/api/v1alpha1"
)

// ClusterContext is a Go context used with a LibvirtCluster.
type ClusterContext struct {
	context.Context
	Cluster        *clusterv1.Cluster
	LibvirtCluster *infrav1.LibvirtCluster
	PatchHelper    *patch.Helper
	Logger         logr.Logger
}

// String returns LibvirtClusterGroupVersionKind LibvirtClusterNamespace/LibvirtClusterClusterName.
func (c *ClusterContext) String() string {
	return fmt.Sprintf("%s %s/%s", c.LibvirtCluster.GroupVersionKind(), c.LibvirtCluster.Namespace, c.LibvirtCluster.Name)
}

// Patch updates the object and its status on the API server.
func (c *ClusterContext) Patch() error {
	// always update the readyCondition.
	conditions.SetSummary(c.LibvirtCluster,
		conditions.WithConditions(
			infrav1.LibvirtAvailableCondition,
		),
	)

	return c.PatchHelper.Patch(c, c.LibvirtCluster)
}
